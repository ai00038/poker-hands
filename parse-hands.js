const   _cardConstants = require('./card-constants');
const   suits =  _cardConstants.suits,
        ranks = _cardConstants.ranks,
        handRanks = _cardConstants.handRanks,
        cardRanks = _cardConstants.cardRanks,
        cardSuits = _cardConstants.cardSuits,
        rankedHands = _cardConstants.rankedHands,
        HAND_SIZE = _cardConstants.HAND_SIZE;

const parseCard = (cardString) => {
    let rankAndSuit = cardString.split('');
    let rank = rankAndSuit[0];
    let suit = rankAndSuit[1];
    let card = {
        suit: suit,
        rank: {
            rank: rank,
            value: cardRanks[rank]
        }
    };
    return card;
}

const parseHand = (handString) =>  handString.split(' ').map(parseCard)

module.exports = {
    parseHand: parseHand
}