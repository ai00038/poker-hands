const   _rankHands = require('./rank-hands');
const   rankHandByDetails = _rankHands.rankHandByDetails;

const   _parseHands = require('./parse-hands');
const   parseHand = _parseHands.parseHand;

const   _getHandDetails = require('./get-hand-details');
const   getHandDetails = _getHandDetails.getHandDetails;

const   _rankHandByCard = require('./rank-hand-by-card');
const   rankHandByCard = _rankHandByCard.rankHandByCard;

const   _cardConstants = require('./card-constants');
const   suits =  _cardConstants.suits,
        ranks = _cardConstants.ranks,
        handRanks = _cardConstants.handRanks,
        cardRanks = _cardConstants.cardRanks,
        cardSuits = _cardConstants.cardSuits,
        rankedHands = _cardConstants.rankedHands,
        HAND_SIZE = _cardConstants.HAND_SIZE;

const rankHand = (handString) => {
    const hand = parseHand(handString);
    let handDetails = getHandDetails(hand);
    let handRankedByDetails = rankHandByDetails(handDetails);
    let cardOfRankedHand = rankHandByCard(hand, handRankedByDetails);
    console.log(handString, ' -> ', handRankedByDetails, ' -> ', reformatCard(cardOfRankedHand));
}

const reformatCard = (card) => card.rank.rank + card.suit

const app = () => {
    const theHands = [
        '4H 3H 5H 7H 6H', // straight flush 7H
        'KD KC KH KS QH', // four of a kind
        'KD KC KH 2H 3H', // three of a kind
        'QA QC QH KS KH', // full house
        'KH KD QH QD AH', // two pairs
        'KH KD 2H 3H 4H', // pair
        '2D 3H 4H 5H 6H', // straight
        '2C 3H 4S 8C AH', // high card AH
        '2S 8S AS QS 3S', // flush QS
        '2H 3D 5S 9C KD', // high card KD
    ];

    theHands.forEach(rankHand);
}

app();