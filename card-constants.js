const suits = ['C', 'D', 'H', 'S'];
const ranks = [2, 3, 4, 5, 6, 7, 8, 9, 10, 'J', 'Q', 'K', 'A'];
const handRanks = [
    'HIGH_CARD', 'PAIR', 'TWO_PAIRS', 'THREE_OF_A_KIND', 'STRAIGHT',
    'FLUSH', 'FULL_HOUSE', 'FOUR_OF_A_KIND', 'STRAIGHT_FLUSH' 
];

const createCardRanks = () => {
    let rankMap = {};
    ranks.forEach((rank, howHigh) => rankMap[rank] = howHigh);
    return rankMap;
}

const createCardSuits = () => {
    let suitMap = {};
    suits.forEach(suit => suitMap[suit] = suit);
    return suitMap;
}

const createRankedHands = () => {
    let rankedHandsMap = {};
    handRanks.forEach( (handRank, howHigh) => rankedHandsMap[handRank] = howHigh);
    return rankedHandsMap;
}

const cardRanks = createCardRanks();
const cardSuits = createCardSuits();
const rankedHands = createRankedHands();

const HAND_SIZE = 5;

module.exports = {
    suits: suits,
    ranks: ranks,
    handRanks: handRanks,
    cardRanks: cardRanks,
    cardSuits: cardSuits,
    rankedHands: rankedHands,
    HAND_SIZE: HAND_SIZE
};