const   _cardConstants = require('./card-constants');
const   suits =  _cardConstants.suits,
        ranks = _cardConstants.ranks,
        handRanks = _cardConstants.handRanks,
        cardRanks = _cardConstants.cardRanks,
        cardSuits = _cardConstants.cardSuits,
        rankedHands = _cardConstants.rankedHands,
        HAND_SIZE = _cardConstants.HAND_SIZE;

const suitFrequenciesEqual = (handDetails, requiredFrequency) => {
    return frequenciesEqual(handDetails, requiredFrequency, 'suitFrequencies');
}

const rankFrequenciesEqual = (handDetails, requiredFrequency) => {
    return frequenciesEqual(handDetails, requiredFrequency, 'rankFrequencies');
}

const frequenciesEqual = (handDetails, requiredFrequency, frequencyType) => {
    let frequencies = handDetails[frequencyType];

    for (key in frequencies) {
        if (frequencies[key] === requiredFrequency) {
            return true;
        }
    }

    return false;
}

const isTwoPairs = (handDetails) => {
    let howManyPairs = 0;

    let rankFrequencies = handDetails.rankFrequencies;
    for (rfKey in rankFrequencies) {
        let rfValue = rankFrequencies[rfKey];
        howManyPairs += (rfValue === 2) ? 1 : 0 ;
    }

    return howManyPairs === 2;
}

const isPair = (handDetails) => rankFrequenciesEqual(handDetails, 2)
const isThreeOfAKind = (handDetails) => rankFrequenciesEqual(handDetails, 3)
const isStraight = (handDetails) => handDetails.hasFiveInRow
const isFlush = (handDetails) => suitFrequenciesEqual(handDetails, HAND_SIZE);
const isFullHouse = (handDetails) => isThreeOfAKind(handDetails) && isPair(handDetails)
const isFourOfAKind = (handDetails) => rankFrequenciesEqual(handDetails, 4)
const isStraightFlush = (handDetails) => isStraight(handDetails) && isFlush(handDetails)

const rankHandByDetails = (handDetails) => {
    if (isStraightFlush(handDetails)) return 'STRAIGHT_FLUSH';
    if (isFourOfAKind(handDetails)) return 'FOUR_OF_A_KIND';
    if (isFullHouse(handDetails)) return 'FULL_HOUSE';
    if (isFlush(handDetails)) return 'FLUSH';
    if (isStraight(handDetails)) return 'STRAIGHT';
    if (isThreeOfAKind(handDetails)) return 'THREE_OF_A_KIND';
    if (isTwoPairs(handDetails)) return 'TWO_PAIRS';
    if (isPair(handDetails)) return 'PAIR';
    return 'HIGH_CARD';
}

module.exports = {
    rankHandByDetails: rankHandByDetails
}