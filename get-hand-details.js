const   _cardConstants = require('./card-constants');
const   suits = _cardConstants.suits,
        ranks = _cardConstants.ranks,
        handRanks = _cardConstants.handRanks,
        cardRanks = _cardConstants.cardRanks,
        cardSuits = _cardConstants.cardSuits,
        rankedHands = _cardConstants.rankedHands,
        HAND_SIZE = _cardConstants.HAND_SIZE;

const createEmptyFrequencyTable = (headers) => {
    let frequencyTable = {};

    headers.forEach(header => frequencyTable[header] = 0);

    return frequencyTable;
}

const getRankFrequencies = (hand) => {
    let rankFrequencies = createEmptyFrequencyTable(ranks);

    hand.forEach(card => rankFrequencies[card.rank.rank]++);

    return rankFrequencies;
}

const getSuitFrequencies = (hand) => {
    let suitFrequencies = createEmptyFrequencyTable(suits);

    hand.forEach(hand => suitFrequencies[hand.suit]++);

    return suitFrequencies;
}

const isFiveInRow = (hand) => {
    let consecutive = 1;
    const firstCard = hand[0];

    for (let index = 1; index < hand.length; index++) {
        let nthCard = hand[index];
        consecutive += (nthCard.rank.value - firstCard.rank.value === index) ? 1 : 0;
    }

    return consecutive === HAND_SIZE;
}

const byAscendingRankValue = (card1, card2) =>  card1.rank.value - card2.rank.value

const getHandDetails = (hand) => {
    let handDetails = {};
    hand.sort(byAscendingRankValue);
    handDetails.suitFrequencies = getSuitFrequencies(hand);
    handDetails.rankFrequencies = getRankFrequencies(hand);
    handDetails.hasFiveInRow = isFiveInRow(hand);
    return handDetails;
}

module.exports = {
    getHandDetails: getHandDetails
};