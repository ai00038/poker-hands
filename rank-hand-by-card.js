const   _cardConstants = require('./card-constants');
const   suits =  _cardConstants.suits,
        ranks = _cardConstants.ranks,
        handRanks = _cardConstants.handRanks,
        cardRanks = _cardConstants.cardRanks,
        cardSuits = _cardConstants.cardSuits,
        rankedHands = _cardConstants.rankedHands,
        HAND_SIZE = _cardConstants.HAND_SIZE;

const LAST_INDEX = HAND_SIZE - 1;

const rankByHighestCard = hand => hand[LAST_INDEX]

const rankForPair = hand => hand[LAST_INDEX]

const rankForTwoPairs = (hand) => {
    return hand[LAST_INDEX];
}

const rankForThreeOfAKind = (hand) => {
    return hand[LAST_INDEX];
}

const rankForFullHouse = (hand) => {
    return hand[LAST_INDEX];
}

const rankForFourOfAKind = (hand) => {
    return hand[LAST_INDEX];
}

const handRankMethods = {
    HIGH_CARD: rankByHighestCard,
    PAIR: rankForPair,
    TWO_PAIRS: rankForTwoPairs,
    THREE_OF_A_KIND: rankForThreeOfAKind, 
    STRAIGHT: rankByHighestCard,
    FLUSH: rankByHighestCard, 
    FULL_HOUSE: rankForFullHouse, 
    FOUR_OF_A_KIND: rankForFourOfAKind, 
    STRAIGHT_FLUSH: rankByHighestCard
};

const rankHandByCard = (hand, handRank) => handRankMethods[handRank](hand)

module.exports = {
    rankHandByCard: rankHandByCard
}